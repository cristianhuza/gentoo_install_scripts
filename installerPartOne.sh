#!/bin/bash
#
# Gentoo Installation Automation Part 1

# See project wiki for further explenation of the below flags.
set -o errexit
set -o nounset
set -o pipefail

#Error capture and reporting
err_report() {
  echo "Error on line $1" >&2
}
trap 'err_report $LINENO' ERR

# Declaring Constants
readonly INSTALLER_PARTTWO_FILENAME=installerPartTwo.sh

#Grab the path of this script. Used to identify location of other scripts
MY_PATH="$(dirname $0)"                       # relative
readonly MY_PATH="$( cd ${MY_PATH} && pwd )"  # absolutized and normalized
if [ -z "${MY_PATH}" ] ; then
  exit 1                                      # fail
fi

###############################################################################
# Preparing the OS disk(s)
# Disk will always be GPT Partition Table
# Globals:
#  None
# Arguments:
#  None
# Returns:
#  None
# UEFI Firmware partition layout is:
#   Device    Purpose  FileSystem
# /dev/sda1 = Boot Partition vfat
# /dev/sda2 = Swap Partition Swap
# /dev/sda3 = Root Partition ext4
# BIOS Firmware partition layout is:
#   Device    Purpose  FileSystem
# /dev/sda1 = BIOS GRUB Part
# /dev/sda2 = Boot Partition ext2
# /dev/sda3 = Swap Partition Swap
# /dev/sda4 = Root Partition ext4
###############################################################################
function disk_prep() {

  # Identify user desired swap size
  local swap_desired="SwapDesired"
  read -p 'Desired swap size? In MiBs e.g 4096 for 4GB>' swap_desired

  #Space Calculations
  if [[ "${bios}" == "uefi" ]]; then
    declare -i start_location; start_location=1
    declare -i boot_size; boot_size=128
    declare -i boot_end; boot_end="${start_location}"+"${boot_size}"
    declare -i swap_end; swap_end="${boot_end}"+"${swap_desired}"
    #Create the Partitions
    parted -a optimal --script "${desired_device}" \
     mklabel gpt \
     mkpart primary "${start_location}"MiB "${boot_end}"MiB \
     mkpart primary "${boot_end}"MiB "${swap_end}"MiB \
     mkpart primary "${swap_end}"MiB 100%
    echo "[+] Partitions Created"
    #Label the partitions
    parted -s "${desired_device}" \
     name 1 boot \
     name 2 swap \
     name 3 rootfs
    echo "[+] Partitions Labeled"
    #Set the boot flag
    parted -s "${desired_device}" set 1 boot on
    echo "[+] Boot Flag Set"
    #Create a filesystem on the partitions
    mkfs.fat -F 32 "${desired_device}"1
    mkfs.ext4 "${desired_device}"3
    mkswap "${desired_device}"2
    echo "[+] Installed Filesystems on each partition"
    mount "${desired_device}"3 /mnt/gentoo
    echo "[+] Mounted Root FileSystem"
    echo "Final Partition Layout"
    parted -a optimal --script "${desired_device}" print
  elif [[ "${bios}" == "bios" ]]; then
    declare -i start_location; start_location=1
    declare -i biosGrubEnd; biosGrubEnd=3
    declare -i boot_size; boot_size=128
    declare -i boot_end; boot_end="${start_location}"+"${boot_size}"
    declare -i swap_end; swap_end="${boot_end}"+"${swap_desired}"
    #Create the Partitions
    parted -a optimal --script "${desired_device}" \
     mklabel gpt \
     mkpart primary "${start_location}"MiB "${biosGrubEnd}"MiB \
     mkpart primary "${biosGrubEnd}"MiB "${boot_end}"MiB \
     mkpart primary "${boot_end}"MiB "${swap_end}"MiB \
     mkpart primary "${swap_end}"MiB 100%
    echo "[+] Partitions Created"
    #Label the partitions
    parted -s "${desired_device}" \
     name 1 biosGrub \
     name 2 boot \
     name 3 swap \
     name 4 rootfs
    echo "[+] Partitions Labeled"
    #Set the boot flag
    parted -s "${desired_device}" set 1 bios_grub on
    echo "[+] Boot Flag Set"
    #Create a filesystem on the partitions
    mkfs.ext2 "${desired_device}"2
    mkfs.ext4 "${desired_device}"4
    mkswap "${desired_device}"3
    echo "[+] Instaleld Filesystems on each partition"
    mount "${desired_device}"4 /mnt/gentoo
    echo "[+] Mounted Root FileSystem"
    echo "Final Partition Layout"
    parted -a optimal --script "${desired_device}" print
  fi
}

################################################################################
# Automating stage 3 download and verify steps
# Globals:
#   None
# Arguments:
#   None
# Returns:
#   None
################################################################################
function stage_three_automation() {

  local -r gentoo_key_id=0xBB572E0E2D182910
  local -r gentoo_download_website="http://distfiles.gentoo.org/releases/amd64/autobuilds/current-install-amd64-minimal/"

  #Download the page source for gentoo distfiles
  curl -s "${gentoo_download_website}" > page_source_file

  #Parse HTML source to narrow in on possible file names
  grep "<a href=\"stage3-amd64-2.*\.tar\.xz\">" \
      page_source_file > stage_three_name_file
  grep "<a href=\"stage3-amd64-2.*\.tar\.xz\.DIGESTS\.asc\">" \
      page_source_file > stage_three_digest_file

  #Remove HTML Tags from html sources
  sed -i 's/<[^>]*>//g' stage_three_name_file
  sed -i 's/<[^>]*>//g' stage_three_digest_file

  #Replace all content after tar.xz with tar.xz, removing gibberish.
  sed -i 's/tar.xz.*/tar.xz/' stage_three_name_file
  sed -i 's/tar.xz.DIGESTS.asc.*/tar.xz.DIGESTS.asc/' stage_three_digest_file

  #Remove all spaces
  sed -i 's/ //g' stage_three_name_file
  sed -i 's/ //g' stage_three_digest_file

  #Download the actual files
  wget "${gentoo_download_website}$(cat stage_three_name_file)"
  wget "${gentoo_download_website}$(cat stage_three_digest_file)"

  # Verify Stage 3
  gpg --keyserver hkps.pool.sks-keyservers.net --recv-keys "$gentoo_key_id"
  gpg --verify "$(cat stage_three_digest_file)"
  grep -A1 "# SHA512 HASH" "$(cat stage_three_digest_file)" | head -n2 > sha_file
  if ! sha512sum -c sha_file; then
    echo "Stage 3 sha comparison has failed"
    exit 1
  fi

  # Clean your room
  rm stage_three_name_file stage_three_digest_file page_source_file sha_file
  echo "Stage 3 Downloaded and Verified"
}
################################################################################
# Download and Extract Stage 3
# Primary purpose is to prevent a redownload if the file already exists,
# incase the script is run again due to a previous error.
# Globals:
#   None
# Arguments:
#   None
# Returns:
#   None
################################################################################
function stage_three_install() {

  local -r stage_three_glob=stage3-amd64-*.tar.xz

  if ls ${stage_three_glob} &> /dev/null; then
    echo "Stage3 file found, will not redownload"
  else
    stage_three_automation
  fi

  #Move and Extract the Tarball
  tar xvpf ${stage_three_glob} --xattrs --numeric-owner -C /mnt/gentoo
  echo "[+] Extracted Stage 3 tarball"
}

################################################################################
# Configure Stage 3, Mount FileSystems, and Chroot into Stage 3
# Globals:
#   None
# Arguments:
#   None
# Returns:
#   None
################################################################################
function chrooting() {
  #Configuring Stage 3
  {
    #Set MAKEOPTS
    local -r makeopts="$(lscpu -p \
     | sed "/#.*/ d" \
     | wc -l)"
    echo "MAKEOPTS=\"-j${makeopts}\"" >> /mnt/gentoo/etc/portage/make.conf
    echo "[+] MAKEOPTS set in stage 3"

    #Set Gentoo Mirror
    echo "GENTOO_MIRRORS=\"http://gentoo.cs.utah.edu/\"" \
      >> /mnt/gentoo/etc/portage/make.conf
    echo "[+] Mirror Set in stage 3"
    echo "Current state of /mnt/gentoo/etc/portage/make.conf"
    cat /mnt/gentoo/etc/portage/make.conf

    #Copy over portage repositories
    mkdir /mnt/gentoo/etc/portage/repos.conf
    cp /mnt/gentoo/usr/share/portage/config/repos.conf \
     /mnt/gentoo/etc/portage/repos.conf/gentoo.conf
    echo "[+] Repositories copied over"

    #Copy of dhcpd information to Stage 3
    cp --dereference /etc/resolv.conf /mnt/gentoo/etc/
    echo "[+] DHCP info copied to stage 3"
  }

  #Mount file systems to stage 3
  mount_file_systems /mnt/gentoo

  #Chroot to stage 3
  cp -a "${MY_PATH}"/"${INSTALLER_PARTTWO_FILENAME}" /mnt/gentoo/
  cp "${MY_PATH}"/kernelConfig /mnt/gentoo/
  cp "${MY_PATH}"/firewall /mnt/gentoo
  echo "[+] Files copied To Stage3"

  chroot /mnt/gentoo /bin/bash -c \
    "export BOOT_DEVICE=${desired_device}; \
    export BOOT_PARTITION_NUMBER=${boot_partition_num}; \
    export BOOT_FIRMWARE=${bios}; \
    export SWAP_PARTITION_NUMBER=${swap_partition_num}; \
    export ROOT_PARTITION_NUMBER=${root_partition_num}; \
    export MAKEOPTS_ENV=${makeopts}; \
    ./${INSTALLER_PARTTWO_FILENAME}"
  echo "[+] Chroot Complete"
}

################################################################################
# Mounts all filesystems required to install gentoo.
# Function is used by script, and exposed to user as its usefull on its own.
# Globals:
#   None
# Arguments:
#   None 
# Returns:
#   None
################################################################################
function mount_file_systems() {

  local mount_path=/mnt/gentoo

  if [[ "${bios}" == "uefi" ]]; then
    swapon "${desired_device}"2
  elif [[ "${bios}" == "bios" ]]; then
    swapon "${desired_device}"3
  fi

  echo "[+] Mounted Swap"
  mount --types proc /proc $mount_path/proc
  echo "[+] Mounted Proc to Stage 3"
  mount --rbind /sys $mount_path/sys
  mount --make-rslave $mount_path/sys
  echo "[+] Mounted Sys to Stage 3"
  mount --rbind /dev $mount_path/dev
  mount --make-rslave $mount_path/dev
  echo "[+] Mounted Dev to Stage 3"
  echo "Currently Mounted Drives"
  lsblk
}

################################################################################
# Unmounts all filesystems mounted by this script.
# Function is used by script, and exposed to user as its usefull on its own.
# Globals:
#   None
# Arguments:
#   None
# Returns:
#   None
################################################################################
function unmount_file_systems() {

  swapoff -a
  echo "[-] UnMounted Swap"
  #umount -l /mnt/gentoo/dev{/shm,/pts,}
  umount -lR /mnt/gentoo
  echo "[-] UnMounted all of stage3 gentoo"
  echo "Current Device State"
  lsblk

}

################################################################################
# Pre-script run; requirements enforcement, and variable Initialization.
# Globals:
#   None
# Arguments:
#   None
# Returns:
#   None
################################################################################
function init_script() {

  #Ensure user is root as a lot of the commands we run require root privilidges.
  if [[ "$(id -u)" -ne 0 ]]; then
    echo "Please run as root"
    exit 1
  fi

  # Identify boot firmware
  # Set expected boot_partition based on firmware
  # Set expected swap_partition based on firmware
  # Set expected root_partition based on firmware
  if [[ -d /sys/firmware/efi ]]; then
    bios="uefi"
    boot_partition_num=1
    swap_partition_num=2
    root_partition_num=3
  else
    bios="bios"
    boot_partition_num=2
    swap_partition_num=3
    root_partition_num=4
  fi

  # Which Device to install Gentoo into?
  # Does it exist?
  lsblk
  read -p 'Which device do you wish to install to? Ex /dev/sda >' \
    desired_device
  if [[ ! -b "${desired_device}" ]]; then
	echo "Device you have requested is not found, exiting."
    exit 1
  fi

  # Correct System Time
  echo "Contacting ntp server for time, this may take a few seconds...."
  ntpdate 0.gentoo.pool.ntp.org
  echo "[+] Time Corrected"
}

################################################################################
# Main Function - Script entry point
# Globals:
#   None
# Arguments:
#   None
# Returns:
#   None
################################################################################
function main() {

local bios="uninitialized"               # Boot firmware type
local desired_device="uninitialized"     # Block device to install OS to
local boot_partition_num="uninitialized" # Partition number of /boot
local swap_partition_num="uninitialized" # Partition number of swap
local root_partition_num="uninitialized" # Partition number of /

cat << EOF
Select which section you wish to run.
1 - Disk Preperation
2 - Stage 3 Download, and Verify. No Extraction.
3 - All (Disk Prep, Stage 3 Prep, Chroot, Execute Installer Part 2)
4 - Unmount swap, proc, sys, dev, /mnt/gentoo
5 - Mount swap, proc, sys, dev, /mnt/gentoo
EOF
read -p 'Which section?>' section

case "${section}" in
  1)
     init_script
     disk_prep
     ;;
  2) stage_three_automation ;;
  3)
     init_script
     disk_prep
     stage_three_install
     chrooting
     unmount_file_systems
     echo "Reset system now."
     ;;
  4) unmount_file_systems ;;
  5) mount_file_systems ;;
  *) echo "Good Bye" ;;
esac
}

main
