#!/bin/bash
#
# Gentoo Installation Automation Part 2
set -o errexit
set -o nounset
set -o pipefail

#Error Capture and reporting
err_report() {
  echo "Error on line $1" >&2
}
trap 'err_report $LINENO' ERR

readonly boot_device="${BOOT_DEVICE}"
readonly boot_firmware="${BOOT_FIRMWARE}"
readonly boot_partition_num="${BOOT_PARTITION_NUMBER}"
readonly swap_partition_num="${SWAP_PARTITION_NUMBER}"
readonly root_partition_num="${ROOT_PARTITION_NUMBER}"
readonly makeopts="${MAKEOPTS_ENV}"

################################################################################
# Modify portage related information
# Globals:
#   None
# Arguments:
#   None
# Returns:
#   None
################################################################################
function portage_configuration () {

  # Download Latest Repository
  emerge-webrsync

  # Set Local timezone
  echo "America/Los_Angeles" > /etc/timezone
  emerge --config sys-libs/timezone-data

  # Select USE profile
  # I am OK with default profile
  # eselect profile list
  # read -p "Desired Profile Number?>" selected_profile
  # eselect profile set "${selected_profile}"

  # Download all profile USE requirements
  emerge -q --update --deep --newuse @world

}


################################################################################
# Set up locale requirements
# Globals:
#   PS1
# Arguments:
#   None
# Returns:
#   None
################################################################################
function select_locale () {

  nano -w /etc/locale.gen
  locale-gen
  eselect locale list
  read -rp "Please select a locale " selected_locale
  eselect locale set "${selected_locale}"
  env-update && source /etc/profile

}

################################################################################
# Configuring FSTAB
# This is used both by genkernel to install kernel, and needed by user to boot.
# Globals:
#   boot_device
#   boot_partition_num
#   swap_partition_num
#   root_partition_num
# Arguments:
#   None
# Returns:
#   None
################################################################################
function fstab_configuration() {

  # Identify boot
  if [[ "${boot_firmware}" == "uefi" ]]; then
      local -r boot_fs="vfat"
  else
      local -r boot_fs="ext2"
  fi

  # Placing Boot Drive into fstab
  echo "${boot_device}${boot_partition_num}	/boot   ${boot_fs}		\
    defaults,noauto,noatime		0 1" >> /etc/fstab
  echo "${boot_device}${swap_partition_num}	none	swap	\
    sw	0 0" >> /etc/fstab
  echo "${boot_device}${root_partition_num}	/	ext4	\
    noatime	0 2" >> /etc/fstab
}

################################################################################
# Download, compile and install the kernel
# Globals:
#   None
# Arguments:
#   None
# Returns:
#   None
################################################################################
function kernel_install () {

  # Mount Boot
  mount /boot

  # Download and install kernel and firmware
  emerge -q sys-kernel/gentoo-sources
  cp /kernelConfig /usr/src/linux/.config

  cd /usr/src/linux
  make -j"${makeopts}" && make -j"${makeopts}" modules_install
  make install
}

################################################################################
# Grub configuration namespace.
# Grub requires a efi-64 flag if boot firmware is uefi
# Globals:
#   boot_firmware
# Arguments:
#   None
# Returns:
#   None
################################################################################
function grub_configuration() {
  if [[ "${boot_firmware}" == "uefi" ]]; then
	echo 'GRUB_PLATFORMS="efi-64"' >> /etc/portage/make.conf
  fi
}

################################################################################
# This function will download all needed packages
# Globals:
#   None
# Arguments:
#   None
# Returns:
#   None
################################################################################
function package_installation () {

  # Install and configure cpuid2cpuflags
  emerge -q app-portage/cpuid2cpuflags

  # Install and configure pciutils, this is not used by the script,
  # may be valuable to the user.
  emerge -q sys-apps/pciutils

  # Install and configure netifrc
  emerge -q --noreplace net-misc/netifrc

  # Install system logger
  emerge -q app-admin/sysklogd

  # Install Cron Daemon
  emerge -q sys-process/cronie

  # Install DHCP client
  emerge -q net-misc/dhcpcd

  # Install Grub2
  emerge -q sys-boot/grub

  # Install IPTables
  emerge -q --noreplace net-firewall/iptables
  
  # Install sudo
  emerge -q app-admin/sudo
  
  # Install git
  emerge -q dev-vcs/git
}


################################################################################
# Final configuration before script exit, configuring packages, openrc, network.
# Globals:
#   None
# Arguments:
#   None
# Returns:
#   None
################################################################################
function final_configuration() {

  # cpuid2cpuflags usage
  echo "CPU_FLAGS_X86=\"$(cpuid2cpuflags | awk -F': ' '{print $2}')\"" \
      >> /etc/portage/make.conf

  #Network Configurations
  {
    ## Find out the physical link name used by this system
    local link_name
    link_name="$(ip link show up | awk -F': ' '{print $2}' | sed -n '3p')"

    ## Set network interface to grab IP via DHCP
    echo "config_${link_name}=\"dhcp\"" >> /etc/conf.d/net

    ## Set Host Name
    local host_name="uninitialized"
    read -rp 'Desired Host Name?' host_name
    echo "hostname=${host_name}" > /etc/conf.d/hostname

    ## Automatically start networking at boot
    cd /etc/init.d
    ln -s net.lo net."${link_name}"
    rc-update add net."${link_name}" default
  }

  # run system logger on startup
  rc-update add sysklogd default

  # run cronie on startup
  rc-update add cronie default

  # Install grub
  if [[ "${boot_firmware}" == "uefi" ]]; then
    grub-install --target=x86_64-efi --efi-directory=/boot "${boot_device}"
  elif [[ "${boot_firmware}" == "bios" ]]; then
    grub-install "${boot_device}"
  fi

  # Configure grub
  grub-mkconfig -o /boot/grub/grub.cfg

  # Configure IPTables
  # This needs to be done after restart as the kernel used is minimal CD kernel
  # which does not have the correct modules.

  # Setting root password
  echo "next instruction to run will be 'passwd' ,\
    please enter a strong root password."
  passwd
}

################################################################################
# Main Function - Script entry point
# Globals:
#   None
# Arguments:
#   None
# Returns:
#   None
################################################################################
function main() {

portage_configuration
select_locale
fstab_configuration
kernel_install
grub_configuration
package_installation
final_configuration

}

main
exit
